SYSTEM ENTITY
-------------

CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Recommended Modules
 * Features
 * Integration (with other modules)
 * Installation
 * Future Developments


INTRODUCTION
------------

System entity seeks to provide configuration and export on individual entities,
dubbed "system entities", that should not be created by content managers and
have unique functionality/appearance per entity. The module is based on a machine
name field that provides a semantic unique identifier per entity. This opens the
door for special purpose entities that do not have common configuration or
content layout with other entities as provided with Drupal's entity type/bundle
scheme.


REQUIREMENTS
------------

This module requires the following modules:

 * Machine Name (https://www.drupal.org/project/machine_name)
 * Entity API (https://www.drupal.org/project/entity)


RECOMMENDED MODULES
-------------------

 * Features (https://www.drupal.org/project/features):
 * Context (https://www.drupal.org/project/context):
 * Crumbs (https://www.drupal.org/project/crumbs):
 * Token (https://www.drupal.org/project/token):
 * Draggable Views (https://www.drupal.org/project/draggableviews):
 * Views Bulk Operations (https://www.drupal.org/project/views_bulk_operations):


FEATURES
--------

 * Flexible entity export (with features) - allows exporting individual entities,
   with initial values per field that are saved only on entity creation, fully
   exported field values which blocks content managers from editing and updated
   with future rebuilding of the export, or a combination of both, with ability to
   omit certain field values from export entirely. All configurable with hooks.
 * Management of related content in a single form - extends the node edit form to
   include related content that may be attached to the specific page, such as a
   list (view) of other page elements (nodes)
 * Provides api for system entities
 * Replaces unintuative use of custom blocks and forms for designing and management
   of pages with unique appearance, content, and layout
 * Permissions per system node
 * Hiding admin only fields from content managers per system node
 * Adds a machine name class for specific entity styling
 * Global system entity tokens - system entity field values can be accessed
   anywhere with token support.
 * Context condition to target specific system nodes


INTEGRATION (WITH OTHER MODULES)
--------------------------------

 * Context
 * Token
 * Crumbs
 * Features


INSTALLATION
------------

 * Install as you would normally install a contributed Drupal module. See:
   https://drupal.org/documentation/install/modules-themes/modules-7
   for further information.


FUTURE DEVELOPMENTS
-------------------

 * Drupal 8 port is on the roadmap.
