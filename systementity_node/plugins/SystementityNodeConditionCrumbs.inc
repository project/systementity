<?php

/**
 * @file
 * SystementityNodeConditionCrumbs.inc
 */

/**
 * System node crumbs as a context condition.
 */
class SystementityNodeConditionCrumbs extends context_condition {

  /**
   * Omit condition values. We will provide a custom input form for our conditions.
   */
  public function condition_values() {
    return array();
  }

  /**
   * Condition form.
   */
  public function condition_form($context) {
    $form = array();

    $node_types = node_type_get_types();
    $system_node_types = systementity_node_type_load();
    $options = array();
    foreach ($system_node_types as $type => $values) {
      if (!empty($node_types[$type])) {
        $options[$type] = $node_types[$type]->name;
      }
    }

    $languages = language_list();
    $languages[LANGUAGE_NONE] = array();
    $langs = array('current' => t('Current language'));
    foreach ($languages as $lang => $values) {
      $langs[$lang] = $lang;
    }

    $default_values = $this->fetch_from_context($context, 'values');

    $form['type'] = array(
      '#type' => 'select',
      '#title' => t('System Node Type'),
      '#default_value' => isset($default_values['type']) ? $default_values['type'] : NULL,
      '#options' => $options,
    );

    $form['machine_name'] = array(
      '#type' => 'textfield',
      '#title' => t('Machine Name'),
      '#description' => t("The value in the node's machine name field."),
      '#default_value' => isset($default_values['machine_name']) ? $default_values['machine_name'] : NULL,
    );

    $form['language'] = array(
      '#type' => 'checkboxes',
      '#title' => t('Language'),
      '#default_value' => isset($default_values['language']) ? unserialize($default_values['language']) : array(),
      '#options' => $langs,
      '#description' => t('If none are selected, language filter will not apply.'),
    );

    $form['position'] = array(
      '#title' => t('Crumbs Position'),
      '#type' => 'textfield',
      '#description' => t('Position of the needed system node in the breadcrumb. 0 for root.'),
      '#default_value' => isset($default_values['position']) ? $default_values['position'] : 1,
      '#required' => TRUE,
      '#element_validate' => array('element_validate_integer'),
    );

    return $form;
  }

  /**
   * Condition form submit handler.
   */
  public function condition_form_submit($values) {
    return array(
      'language' => serialize($values['language']),
      'machine_name' => $values['machine_name'],
      'type' => $values['type'],
      'position' => $values['position'],
    );
  }

  /**
   * Options form.
   */
  public function options_form($context) {
    return array();
  }

  /**
   * Context execute.
   */
  public function execute($trail) {
    global $language;
    if ($this->condition_used()) {
      $paths = array_keys($trail);
      foreach ($this->get_contexts() as $context) {
        $condition_values = $this->fetch_from_context($context, 'values');
        if (isset($condition_values['position']) && isset($paths[$condition_values['position']])) {
          $crumbs_path = $paths[$condition_values['position']];
          $node = menu_get_object('node', 1, $crumbs_path);
          if ($node) {
            $machine_name = systementity_node_machine_name($node);
            if (isset($machine_name)) {
              $condition_values['language'] = unserialize($condition_values['language']);
              if (!empty($condition_values['language']['current'])) {
                $condition_values['language'][$language->language] = $language->language;
              };
              $language_condition = FALSE;
              foreach ($condition_values['language'] as $value) {
                if ($value) {
                  $language_condition = TRUE;
                }
              }
              if ($node->type === $condition_values['type'] && (!$language_condition || !empty($condition_values['language'][$node->language]))) {
                if ($machine_name == $condition_values['machine_name']) {
                  $this->condition_met($context);
                }
              }
            }
          }
        }
      }
    }
  }

}
