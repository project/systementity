<?php

/**
 * @file
 * systementity.api.php
 */

/**
 * Info hook for declaring system entity types and their settings.
 *
 * @return array
 *   Info array in the structure, with the following structure:
 *   - entity type
 *     - bundle //use SYSTEMENITY_NO_BUNDLE if entity type does not have bundles
 *       - machine name field (required)
 *         The associated machine name field for the system entity
 *       - config field (optional)
 *         The associated config field which has it's variables defined in another
 *         hook, see hook_systementity_settings(). Requires configfield module.
 *       - admin fields (optional)
 *         Fields that will be hidden to everyone without administer system nodes
 *         permission. Requires systementity_node_ui.
 *       - export (optional)
 *         - fields (optional, list of fields to export, supports also any
 *           property of the entity)
 *         - alias (optional, true/false)
 *         - parent (optional, requires systemtaxonomy)
 *         - initial value fields (optional, list of fields to export only creation)
 *           Entity fields that will be exported/imported with initial values.
 */
function hook_systementity_type_info() {
  $types['node']['my_system_entity'] = array(
    'machine name field' => 'field_msp_machine_name',
    'config field' => 'field_msp_config',
    'admin fields' => array('body', 'field_msp_machine_name'),
    'export' => array(
      'fields' => array('body', 'promote'),
      'alias' => TRUE,
    ),
  );

  $types['node']['my_system_item'] = array(
    'machine name field' => 'field_msi_machine_name',
  );

  return $types;
}

/**
 * Provides an array of settings for any system entity.
 *
 * @return array
 *   System entity settings array, with following structure:
 *   - entity type
 *     - bundle //use SYSTEMENITY_NO_BUNDLE if entity type does not have bundles
 *       - machine name
 *         - variables //optional, requires configfield
 *         - tabs //optional, array, requires systementity_node_ui, see note blow
 *         - links //optional, array, requires systementity_node_ui
 *         - export //optional
 *           - fields //optional, array of field names
 *           - alias //optional, boolean
 *           - parent //optional, boolean, requires systemtaxonomy
 *         - admin fields //optional, requires systementity_node_ui
 *   Variables are (nested) list of config field variables which are editable in
 *   the system entity edit form. Requires configfield. Variables can be made of
 *   regular form api fields, or use one of the shortcut field apis, which can
 *   also have their properies overriden with any additional '#' properties.
 *   All shortcut fields can have the following properties:
 *   - type
 *   - title
 *   - description
 *   - multi // for duplicating variables (with numeric index)
 *   - multi title callback // requires multi property, custom callback for multi
 *     The callback arguments are:
 *       - $var // variable settings
 *       - $value // variable value
 *       - $delta // delta of the multi item
 *     Returns a string as the multi item title.
 *   - default value
 *   - admin only
 *   The different types and their special properties are:
 *   - group //this is not actually a field but a container for nested variables
 *   - image // @deprecated. See "media" instead. Requires media module
 *   - media // Requires media module
 *   - textfield
 *   - text area
 *   - wysiwyg //requires wysiwyg
 *   - checkbox
 *   - radio
 *   - select
 *   - entityreference //requires entityreference_autocomplete
 *    - entity type
 *    - bundles
 *   - youtube //requires media_youtube
 *   - link //requires custom module link_form_element
 *    - title field
 *    - target field
 *   Tabs contain a list of tabs to place in the node edit page, requires systementity_node_ui. The node edit form is
 *   always the default tab. If no tabs are defined, the node edit form will appear
 *   without the tabs. All tabs can have the following properties:
 *   - type
 *   - title
 *   - admin only
 *   The different tab types and their properties include:
 *   - form
 *    - form id //optional
 *    - form callback //optional. used for forms with complex arguments
 *   - view
 *    - view name
 *    - view display
 *    - arguments //optional array
 *   - default manage content //requires systementity_node_ui_views
 *    - bundles
 *   - default sort view //requires systementity_node_ui_views
 *    - sort id
 *    - bundles
 *   Default manage content is a view that comes with systementity_ui which has it's
 *   content type filter overridden by the bundles property. Same with default sort
 *   view, except that one uses draggable views.
 *   Links are a list of action links that appear at the top of the node edit page, for pages
 *   that cannot be included as tabs. Requires systementity_node_ui. Links have the following properties:
 *   - title
 *   - href
 *   - link back //optional, default is FALSE. Adds destination query parameter for
 *   returning to node edit page after form submit.
 */
function hook_systementity_settings() {
  $system_entities = array();

  $system_entities['node']['my_system_entity']['test_sp']['variables'] = array(
    'var1' => array(
      // Requires entityreference_autocomplete.
      'type' => 'entityreference',
      'title' => t('Entity Reference'),
      'entity type' => 'node',
      'bundles' => array('milestone'),
      'admin only' => TRUE,
    ),
    'var2' => array(
      // Requires media.
      'type' => 'media',
      'title' => t('Media'),
    ),
    'var3' => array(
      'type' => 'textfield',
      'title' => t('Textfield multi'),
      'multi' => 3,
    ),
    'var4' => array(
      'type' => 'textarea',
      'title' => t('Textarea'),
    ),
    'var5' => array(
      // Requires wysiwyg.
      'type' => 'wysiwyg',
      'title' => t('Wysiwyg'),
      'text_format' => 'full_html',
    ),
    'var6' => array(
      'type' => 'group',
      'title' => t('Group'),
      'children' => array(
        'var6-a' => array(
          'type' => 'checkbox',
          'title' => t('Checkbox'),
        ),
        'var6-b' => array(
          'type' => 'radio',
          'title' => t('Radio'),
          'options' => array('one' => '1', 'two' => '2'),
        ),
        'var6-c' => array(
          'type' => 'select',
          'title' => t('Select'),
          'options' => array('one' => '1', 'two' => '2'),
        ),
      ),
    ),
    'var7' => array(
      // Requires media_youtube.
      'type' => 'youtube',
      'title' => t('Youtube'),
    ),
    'var8' => array(
      'type' => 'group',
      'title' => t('Nested Group'),
      'children' => array(
        'a' => array(
          'type' => 'textfield',
          'title' => t('var1'),
        ),
        'var6-d' => array(
          'type' => 'group',
          'title' => t('Nested Group'),
          'children' => array(
            'a' => array(
              'type' => 'textfield',
              'title' => t('var1'),
            ),
            'b' => array(
              'type' => 'textfield',
              'title' => t('var2'),
            ),
            'c' => array(
              'type' => 'textfield',
              'title' => t('var3'),
            ),
          ),
        ),
      ),
    ),
    'var9' => array(
      'type' => 'group',
      'title' => t('Group multi'),
      'multi title callback' => 'customization_my_multi_var_title_callback',
      'children' => array(
        'var9-a' => array(
          'type' => 'group',
          'title' => t('Group multi b'),
          'multi' => 3,
          'children' => array(
            'a' => array(
              'type' => 'textfield',
              'title' => t('var1'),
            ),
            'b' => array(
              'type' => 'textfield',
              'title' => t('var2'),
            ),
            'c' => array(
              'type' => 'textfield',
              'title' => t('var3'),
            ),
          ),
        ),
      ),
    ),
    'var10' => array(
      // Requires custom module link_form_element.
      'type' => 'link',
      'title' => t('Link'),
      'title field' => TRUE,
      'target field' => TRUE,
    ),
    'var11' => array(
      'type' => 'textfield',
      'title' => t('Textfield with default'),
      'default value' => 'This is the "var11" textfield value',
    ),
  );
  $system_entities['node']['my_system_entity']['test_sp']['tabs'] = array(
    array(
      'type' => 'default manage content',
      'bundles' => array('my_system_item'),
    ),
    array(
      'type' => 'default sort view',
      'sort id' => 'my_default_sort',
      'bundles' => array('my_system_item'),
    ),
    array(
      'type' => 'form',
      'form id' => 'systementity_example_my_custom_form',
      'title' => t('Custom form'),
      'admin only' => TRUE,
    ),
    array(
      'type' => 'view',
      'view name' => 'custom_view',
      'view display' => 'page',
      'title' => t('Custom view'),
      'admin only' => TRUE,
    ),
  );

  $system_entities['node']['my_system_entity']['test_sp']['links'] = array(
    array(
      'title' => t('Custom Link'),
      'href' => 'node/2',
    ),
  );

  $system_entities['node']['my_system_item']['item_1']['export']['fields'] = array('body');

  return $system_entities;
}

/**
 * Alter/add content during system entity view.
 *
 * @param string $machine_name
 *   Machine name.
 * @param object $entity
 *   System entity object.
 * @param array $content
 *   Render array of the entity.
 * @param string $view_mode
 *   View mode.
 */
function hook_systementity__node__my_system_entity__view($machine_name, $entity, &$content, $view_mode) {
  switch ($machine_name) {
    case 'test_sp':
      if ($view_mode == 'full') {
        $content['my_content'] = array(
          '#markup' => systementity_var_get('node', $entity, 'var11', 'This is the "var11" textfield value'),
        );
        $content['my_content2'] = array(
          '#markup' => systementity_node_ui_embed_default_content_view($entity, 'my_default_sort', 'teaser'),
        );
      }
      break;
  }
}

/**
 * Alter system entity export array during features export.
 *
 * Useful for special fields/properties of the entity that hold references
 * as values to other entities, which would not export import well between
 * production/dev/staging, and should be used in conjunction with
 * hook_systementity_import_alter() and hook_systementity_import_finished().
 *
 * @param array $export
 *   Export array.
 * @param array $context
 *   Context array with:
 *   - entity type
 *   - bundle
 *   - machine name
 *   - language
 *   - entity (object)
 *   - type settings
 *   - entity settings
 *   - existing export.
 *
 * @see systementity_node_systementity_export_alter()
 * @see systemtaxonomy_systementity_export_alter()
 */
function hook_systementity_export_alter(&$export, $context) {
  if ($context['entity type'] == 'node' && $context['bundle'] == 'my_system_entity') {
    $node = $context['entity'];
    $export['promote'] = $node->promote;
  }
}

/**
 * Alter system entity before entity save during features import (revert).
 *
 * @param object $entity
 *   System entity object.
 * @param array $context
 *   Context array with:
 *   - entity type
 *   - bundle
 *   - machine name
 *   - language
 *   - export
 *   - op ('create', 'update', or 'none').
 *
 * @see systementity_node_systementity_import_alter()
 */
function hook_systementity_import_alter(&$entity, $context) {
  if ($context['entity type'] == 'node' && $context['bundle'] == 'my_system_entity') {
    $entity->promote = $context['export']['promote'];
  }
}

/**
 * Event for after system entity save during features import (revert).
 *
 * @param object $entity
 *   System entity object.
 * @param array $context
 *   Context array with:
 *   - entity type
 *   - bundle
 *   - machine name
 *   - language
 *   - export
 *   - op ('create', 'update', or 'none').
 *
 * @see systementity_node_systementity_import_alter()
 */
function hook_systementity_import($entity, $context) {
  if ($context['op'] == 'create' && $context['entity type'] == 'node' && $context['bundle'] == 'my_system_entity' && $context['machine name'] == 'test_sp') {
    variable_set('some_variable', $entity->nid);
  }
}

/**
 * Event after import is done, with list of imported system entities and their context.
 *
 * @param array $imported
 *   Array of entities and contexts (see hook_systementity_import) in the import,
 *   with the structure of:
 *   - entity type
 *     - bundle
 *       - machine name
 *         - langcode
 *           - entity
 *             The imported system entity object
 *           - context
 *             - entity type
 *             - bundle
 *             - machine name
 *             - language
 *             - export
 *             - op ('create', 'update', or 'none').
 *
 * @see systementity_node_systementity_import_finished()
 * @see systemtaxonomy_systementity_import_finished()
 */
function hook_systementity_import_finished($imported) {
  if ($imported['node']['my_system_entity']['test_sp']['en']['context']['op'] == 'created') {
    $node = $imported['node']['my_system_entity']['test_sp']['en']['entity'];
    $tnid = $imported['node']['my_system_entity']['test_sp']['he']['entity']->nid;
    $node->tnid = $tnid;
    node_save($node);
  }
}

/**
 * Entity type helper info.
 *
 * Provides helper info for handling bundle entities of an entity type, such as
 * taxonomy vocabularies, which are id based as opposed to machine name.
 *
 * @return array
 *   Array of metadata per entity type
 *   - entity type
 *     - keys
 *       Override entity keys.
 *     - bundle machine name callback
 *       Callback for getting bundle machine name from id (eg. taxonomy).
 *     - bundle id callback
 *       Callback for getting bundle ide from machine name (eg. taxonomy).
 */
function hook_systementity_type_helper_info() {
  return array(
    'taxonomy_term' => array(
      'keys' => array('bundle' => 'vid'),
      'bundle machine name callback' => 'systemtaxonomy_bundle_machine_name',
      'bundle id callback' => 'systemtaxonomy_bundle_id',
    ),
  );
}
