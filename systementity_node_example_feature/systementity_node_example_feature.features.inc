<?php
/**
 * @file
 * systementity_node_example_feature.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function systementity_node_example_feature_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "context" && $api == "context") {
    return array("version" => "3");
  }
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_views_api().
 */
function systementity_node_example_feature_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}

/**
 * Implements hook_node_info().
 */
function systementity_node_example_feature_node_info() {
  $items = array(
    'my_system_item' => array(
      'name' => t('My System Item'),
      'base' => 'node_content',
      'description' => t('A system item that uses the system_page module.'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
    'my_system_page' => array(
      'name' => t('My System Page'),
      'base' => 'node_content',
      'description' => t('System page example.'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}

/**
 * Implements hook_systementity_feature_entities_default().
 */
function systementity_node_example_feature_systementity_feature_entities_default() {
  return array(
    'node|my_system_item|item_1' => array(
      'en' => array(
        'initial values' => array(
          'title' => 'Item 1',
        ),
        'fields' => array(
          'field_msi_machine_name' => array(
            'und' => array(
              0 => array(
                'value' => 'item_1',
              ),
            ),
          ),
          'body' => array(
            'und' => array(
              0 => array(
                'value' => '<p>test export</p>',
                'summary' => '',
                'format' => 'full_html',
                'safe_value' => '<p>test export</p>
',
                'safe_summary' => '',
              ),
            ),
          ),
        ),
        'initial alias' => 'mysystemitem/item-1',
        'tnid' => 'he',
      ),
      'he' => array(
        'initial values' => array(
          'title' => 'Item 1 - he',
        ),
        'fields' => array(
          'field_msi_machine_name' => array(
            'und' => array(
              0 => array(
                'value' => 'item_1',
              ),
            ),
          ),
          'body' => array(),
        ),
        'initial alias' => 'mysystemitem/item-1-he',
        'tnid' => 'he',
      ),
    ),
    'node|my_system_item|item_2' => array(
      'en' => array(
        'initial values' => array(
          'title' => 'Item 2',
        ),
        'fields' => array(
          'field_msi_machine_name' => array(
            'und' => array(
              0 => array(
                'value' => 'item_2',
              ),
            ),
          ),
        ),
        'initial alias' => 'mysystemitem/item-2',
        'tnid' => 'en',
      ),
    ),
    'node|my_system_page|test_sp' => array(
      'en' => array(
        'initial values' => array(
          'title' => 'System Page',
        ),
        'fields' => array(
          'field_msp_machine_name' => array(
            'und' => array(
              0 => array(
                'value' => 'test_sp',
              ),
            ),
          ),
          'body' => array(),
          'promote' => 1,
        ),
        'initial alias' => 'mysystempage/system-page',
        'alias' => 'mysystempage/system-page',
        'tnid' => 'en',
      ),
      'he' => array(
        'initial values' => array(
          'title' => 'System Page - he',
        ),
        'fields' => array(
          'field_msp_machine_name' => array(
            'und' => array(
              0 => array(
                'value' => 'test_sp',
              ),
            ),
          ),
          'body' => array(),
          'promote' => 1,
        ),
        'initial alias' => 'mysystempage/system-page-he',
        'alias' => 'mysystempage/system-page-he',
        'tnid' => 'he',
      ),
    ),
  );
}
