<?php

/**
 * @file
 * systementity.tokens.inc
 */

/**
 * Implements hook_token_info().
 */
function systementity_token_info() {
  $types = array();
  $tokens = array();

  $types['systementity'] = array(
    'name' => t("System Entity"),
    'description' => t("System entity types."),
  );

  $system_entity_types = systementity_type_load();
  $languages = array('current_language' => array(), LANGUAGE_NONE => array());
  $languages += language_list();
  $languages = array_keys($languages);

  // For each secion add a node token group.
  foreach ($system_entity_types as $entity_type => $bundles) {

    $lang_key = systementity_key($entity_type, 'language');

    $types["systementity|$entity_type"] = array(
      'name' => t("System Entity Type"),
      'description' => t("System entity type."),
      'needs-data' => 'systementity',
    );

    $types["systementity_language|$entity_type"] = array(
      'name' => t("System Entity Language"),
      'description' => t("System entity languages."),
      'needs-data' => "systementity|$entity_type",
    );

    $tokens['systementity'][$entity_type] = array(
      'name' => $entity_type,
      'description' => t('System entity type @type', array(
        '@type' => $entity_type,
      )),
      'type' => "systementity|$entity_type",
    );

    foreach ($bundles as $bundle => $settings) {
      $tokens["systementity|$entity_type"][$bundle] = array(
        'name' => $bundle,
        'description' => t('System entity bundle'),
        'type' => "systementity_language|$entity_type",
        'dynamic' => TRUE,
      );
    }

    $entity_languages = $lang_key ? $languages : array(SYSTEMENTITY_NO_LANGUAGE);
    foreach ($entity_languages as $langcode) {
      $tokens["systementity_language|$entity_type"][$langcode] = array(
        'name' => $langcode,
        'description' => t('System entity language'),
        'type' => $entity_type,
      );
    }
  }

  return array(
    'types' => $types,
    'tokens' => $tokens,
  );
}

/**
 * Implements hook_tokens().
 */
function systementity_tokens($type, $tokens, array $data = array(), array $options = array()) {
  global $language;

  $replacements = array();

  if ($type == 'systementity') {
    foreach ($tokens as $token => $original) {
      list($entity_type, $bundle, $machine_name, $lang) = explode(':', $token);
      $entity_token = substr($token, strlen(implode(':', array(
        $entity_type,
        $bundle,
        $machine_name,
        $lang,
      ))) + 1);
      if ($entity_type && $bundle && $machine_name && $machine_name && $lang && $entity_token) {
        $langcode = $lang == 'current_language' ? $language->language : $lang;
        $entity = systementity_load_entity($entity_type, $bundle, $machine_name, $langcode);
        if ($entity) {
          $replacements += token_generate($entity_type, array($entity_token => $original), array($entity_type => $entity), $options);
        }
      }
    }
    systementity_clear_tokens($tokens, $replacements);
  }

  return $replacements;
}

/**
 * Clears unreplaced tokens.
 *
 * @param array $tokens
 *   The tokens array..
 * @param array $replacements
 *   The replacements array.
 * @param bool $clear
 *   The clear flag is used to set if unreplaced tags should be cleared..
 *   defaults to TRUE.
 */
function systementity_clear_tokens($tokens, &$replacements, $clear = TRUE) {

  // Checks if the clear option has been flagged.
  if ($clear) {

    // Loop threw all the tokens.
    foreach ($tokens as $original) {

      // Check if the token original value exists in the replacements array.
      if (!isset($replacements[$original])) {

        // Replace the token in the replacements array to an empty string.
        $replacements[$original] = '';
      }
    }
  }
}
