<?php

/**
 * @file
 * systementity.features.inc
 */

/**
 * Implements hook_features_export_options().
 */
function systementity_entities_features_export_options() {
  $options = array();

  $system_entity_types = systementity_type_load();
  foreach ($system_entity_types as $entity_type => $bundles) {
    foreach ($bundles as $bundle => $info) {
      $machine_names = systementity_machine_name_list($entity_type, $bundle);
      foreach ($machine_names as $machine_name => $titles) {
        $options["$entity_type|$bundle|$machine_name"] = "$entity_type - $bundle  - $machine_name";
      }
    }
  }

  return $options;
}

/**
 * Implements hook_features_export().
 */
function systementity_entities_features_export($data, &$export, $module_name) {
  $export['dependencies']['systementity'] = 'systementity';
  $pipe = array();
  foreach ($data as $component) {
    $export['features']['systementity_entities'][$component] = $component;

    list($entity_type, $bundle, $machine_name) = explode("|", $component);
    $context = array(
      'entity type' => $entity_type,
      'bundle' => $bundle,
      'machine name' => $machine_name,
    );
    drupal_alter('systementity_export_dependencies', $export['dependencies'], $pipe, $context);
  }
  return $pipe;
}

/**
 * Implements hook_features_export_render().
 */
function systementity_entities_features_export_render($module_name, $data, $export = NULL) {
  $code = array();
  $system_entity_types = systementity_type_load();
  $system_entity_configs = systementity_load_settings();
  $existing_exports = module_invoke_all('systementity_feature_entities_default');

  $languages = language_list();
  $languages[LANGUAGE_NONE] = array();

  foreach ($data as $component) {
    list($entity_type, $bundle, $machine_name) = explode('|', $component);
    $type_settings = !empty($system_entity_types[$entity_type][$bundle]) ? $system_entity_types[$entity_type][$bundle] : array();
    $entity_settings = !empty($system_entity_configs[$entity_type][$bundle][$machine_name]) ? $system_entity_configs[$entity_type][$bundle][$machine_name] : array();
    $machine_name_field = $type_settings['machine name field'];
    $label_key = systementity_key($entity_type, 'label');
    $language_key = systementity_key($entity_type, 'language');
    if (!field_info_field($machine_name_field)) {
      continue;
    }
    $entity_languages = $language_key ? array_keys($languages) : array(SYSTEMENTITY_NO_LANGUAGE);
    $values = array();
    foreach ($entity_languages as $langcode) {
      $system_entity = systementity_load_entity($entity_type, $bundle, $machine_name, $langcode);
      if ($system_entity) {
        $values[$langcode] = array();
        // Initial values are only used on entity creation and not in
        // any further exports/imports.
        if (isset($existing_exports[$component][$langcode]['initial values'][$label_key])) {
          $values[$langcode]['initial values'][$label_key] = $existing_exports[$component][$langcode]['initial values'][$label_key];
        }
        elseif (!empty($label_key) && isset($system_entity->$label_key)) {
          $values[$langcode]['initial values'][$label_key] = $system_entity->$label_key;
        }

        // Initial values for fields.
        if (!empty($type_settings['export']['initial value fields'])) {
          foreach ($type_settings['export']['initial value fields'] as $field) {
            if (isset($system_entity->$field)) {
              if (isset($existing_exports[$component][$langcode]['initial values'][$field])) {
                $values[$langcode]['initial values'][$field] = $existing_exports[$component][$langcode]['initial values'][$field];
              }
              else {
                $values[$langcode]['initial values'][$field] = $system_entity->$field;
              }
            }
          }
        }

        // Export fields if defined in system entity type settings.
        $values[$langcode]['fields'][$machine_name_field] = $system_entity->$machine_name_field;
        if (!empty($type_settings['export']['fields'])) {
          foreach ($type_settings['export']['fields'] as $field) {
            if (isset($system_entity->$field)) {
              $values[$langcode]['fields'][$field] = $system_entity->$field;
            }
          }
        }

        // Export fields if defined in specific system entity settings.
        if (!empty($entity_settings['export']['fields'])) {
          foreach ($entity_settings['export']['fields'] as $field) {
            if (isset($system_entity->$field)) {
              $values[$langcode]['fields'][$field] = $system_entity->$field;
            }
          }
        }

        if (module_exists('path')) {
          $uri = entity_uri($entity_type, $system_entity);
          $alias = drupal_get_path_alias($uri['path'], $langcode);
          // Initial alias, same as initial values above but for alias.
          if (!empty($existing_exports[$component][$langcode])) {
            if (isset($existing_exports[$component][$langcode]['initial alias'])) {
              $values[$langcode]['initial alias'] = $existing_exports[$component][$langcode]['initial alias'];
            }
          }
          elseif ($alias && $alias != $uri['path']) {
            $values[$langcode]['initial alias'] = $alias;

          }

          // Export current alias if defined in system entity type settings.
          if (!empty($type_settings['export']['alias']) && $alias && $alias != $uri['path']) {
            $values[$langcode]['alias'] = $alias;
          }

          // Export current alias if defined in specific system entity settings.
          if (!empty($entity_settings['export']['alias']) && $alias && $alias != $uri['path']) {
            $values[$langcode]['alias'] = $alias;
          }
        }

        $context = array(
          'entity type' => $entity_type,
          'bundle' => $bundle,
          'machine name' => $machine_name,
          'language' => $langcode,
          'entity' => $system_entity,
          'type settings' => $type_settings,
          'entity settings' => $entity_settings,
          'existing export' => !empty($existing_exports[$component][$langcode]) ? $existing_exports[$component][$langcode] : array(),
        );

        drupal_alter('systementity_export', $values[$langcode], $context);
      }
    }
    $code[$component] = $values;
  }
  $code = "  return " . features_var_export($code, '  ') . ';';

  return array('systementity_feature_entities_default' => $code);
}

/**
 * Implements hook_features_revert().
 */
function systementity_entities_features_revert($module) {
  systementity_entities_features_rebuild($module);
}

/**
 * Implements hook_features_rebuild().
 */
function systementity_entities_features_rebuild($module) {
  $items = module_invoke($module, 'systementity_feature_entities_default');
  $imported = array();

  foreach ($items as $component => $data) {
    list($entity_type, $bundle, $machine_name) = explode('|', $component);
    foreach ($data as $langcode => $export) {
      $context = array(
        'entity type' => $entity_type,
        'bundle' => $bundle,
        'machine name' => $machine_name,
        'language' => $langcode,
        'export' => $export,
      );
      // Update system entity if entity already exists, otherwise create new entity.
      $system_entity = systementity_load_entity($entity_type, $bundle, $machine_name, $langcode);
      if ($system_entity) {
        $context['op'] = 'update';

        $old_entity = clone $system_entity;
        if (!empty($export['fields'])) {
          foreach ($export['fields'] as $field => $value) {
            $system_entity->$field = $value;
          }
        }

        if (module_exists('path')) {
          $uri = entity_uri($entity_type, $system_entity);
          if (!empty($export['alias']) && $export['alias'] != drupal_get_path_alias($uri['path'], $langcode)) {
            $path = array(
              'source' => $uri['path'],
              'alias' => $export['alias'],
              'language' => $langcode,
            );
            path_save($path);
          }
        }

        if ($system_entity == $old_entity) {
          $context['op'] = 'none';
        }

        // Hook for altering entity before update.
        drupal_alter('systementity_import', $system_entity, $context);

        if ($system_entity != $old_entity) {
          $context['op'] = 'update';
          entity_save($entity_type, $system_entity);
        }

        // Hook for system entity updating.
        module_invoke_all('systementity_import', $system_entity, $context);
      }
      else {
        $context['op'] = 'create';
        $bundle_key = systementity_key($entity_type, 'bundle');
        $language_key = systementity_key($entity_type, 'language');
        if ($bundle_key) {
          $system_entity = entity_create($entity_type, array($bundle_key => systementity_bundle_id($entity_type, $bundle)));
        }
        else {
          $system_entity = entity_create($entity_type, array());
        }
        if ($language_key) {
          $system_entity->$language_key = $langcode;
        }

        // Use initial values in entity creation.
        if (!empty($export['initial values'])) {
          foreach ($export['initial values'] as $field => $value) {
            $system_entity->$field = $value;
          }
        }
        if (!empty($export['fields'])) {
          foreach ($export['fields'] as $field => $value) {
            $system_entity->$field = $value;
          }
        }

        // Hook for altering entity before creation.
        drupal_alter('systementity_import', $system_entity, $context);

        entity_save($entity_type, $system_entity);

        // Save alias.
        if (module_exists('path')) {
          $uri = entity_uri($entity_type, $system_entity);
          $alias = !empty($export['alias']) ? $export['alias'] : (!empty($export['initial alias']) ? $export['initial alias'] : FALSE);
          $path = array(
            'source' => $uri['path'],
            'alias' => $alias,
            'language' => $langcode,
          );
          path_save($path);
        }

        module_invoke_all('systementity_import', $system_entity, $context);
      }

      $imported[$entity_type][$bundle][$machine_name][$langcode] = array(
        'entity' => $system_entity,
        'context' => $context,
      );
    }
  }

  if (!empty($imported)) {
    module_invoke_all('systementity_import_finished', $imported);
  }
}
